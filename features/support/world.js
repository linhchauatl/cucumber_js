// On Mac, Linux: export NODE_PATH=/usr/local/lib/node_modules
// Run selenium server: java -jar selenium-server-standalone-2.48.2.jar -port 8910
// or phantomjs -w

// If you run test with cucumber-mink, you don't need the code below.
// See https://www.npmjs.com/package/cucumber-mink


// var zombie = require('zombie');
// function World() {
//   this.browser = new zombie(); // this.browser will be available in step definitions

//   this.visit = function (url, callback) {
//     this.browser.visit(url, callback);
//   };
// }

// module.exports = function() {
//   this.World = World;
// };