# These are test to test this rails application running on your localhost: https://github.com/linhchauatl/animals
Feature: Home Page
  Background:
    Given I browse "http://localhost:3000"

  Scenario: Animals Homepage
    Given I am on the homepage
    Then I should see "Dogs list"
    Then I should see "Cats"
    Then I should see "Fish"
    Then I wait 1 seconds

  Scenario: Create new dog succesully
    Given I am on the homepage
    Then I should see "Dogs list"
    And I follow "a[href='/dogs/new']"
    Then I should see "New dog"
    And I fill in the following:
      | input[id='dog_name']     | New dog       |
      | input[id='dog_age']      | 5             |
      | input[id='dog_dog_attr'] | Newly created |
      | input[id='dog_pack_id']  | 1             |
    And I press "Create"
    Then I should see "Dog was successfully created."
    Then I wait 1 seconds

  Scenario: Create new dog failed
    Given I am on the homepage
    Then I should see "Dogs list"
    And I follow "a[href='/dogs/new']"
    Then I should see "New dog"
    And I fill in the following:
      | input[id='dog_name']     | New dog       |
      | input[id='dog_dog_attr'] | Newly created |
      | input[id='dog_pack_id']  | 1             |
    And I press "Create"
    Then I should see "Age can't be blank, Age is not a number"
    Then I wait 5 seconds