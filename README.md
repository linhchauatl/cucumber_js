## cucumber-js demo

This project shows how to write and run BDD tests with [cucumber-js](https://github.com/cucumber/cucumber-js) and [cucumber-mink](https://www.npmjs.com/package/cucumber-mink).<br/>
Detailed information is [**here**](http://hanoian.com/content/index.php/4-using-cucumber-js-to-perform-bdd).